<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH.'libraries/util/CI_Object.php';

class ContaDataBuilder extends CI_Object {

    private $contas = [
        [
        'parceiro' => 'Biqueira', 
        'descricao' => 'droga', 
        'valor' => '1', 
        'mes' => 1, 
        'ano' => 2021, 
        'tipo' => 'pagar'
        ],
        [
            'parceiro' => 'Aluguel', 
            'descricao' => 'teste', 
            'valor' => '1', 
            'mes' => 1, 
            'ano' => 2021, 
            'tipo' => 'pagar'
        ],
        [
            'parceiro' => 'Energia', 
            'descricao' => 'teste', 
            'valor' => '1', 
            'mes' => 1, 
            'ano' => 2021, 
            'tipo' => 'pagar'
        ]
    ];

    public function start()
    {
        $this->load->library('conta');

        foreach ($this->contas as $conta) {
            $this->conta->cria($conta);
        }
    }

    public function clear() {
        $this->db->truncate('conta');
    }
}