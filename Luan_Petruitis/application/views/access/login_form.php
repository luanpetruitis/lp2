<div class="d-flex justify-content-center p-2">
  <div class="d-flex p-2">
    <div class="text-center">

      <h3 class="my-4">Controle Financeiro Pessoal</h3>

      <form class="text-center border border-info rounded p-5" method="POST">
        <p class="h4 mb-4">Entrar</p>

        <input type="email" id="email" name="email" class="form-control mb-4" placeHolder="Email">
        <input type="password" id="senha" name="senha" class="form-control mb-4" placeholder="Senha">

        <button class="btn btn-info btn-block my-4" type="submit">Enviar</button>
        
        <p class="red-text"><?= $error ? "Dados de acesso incorretos" : ''  ?></p>
      </form>

    </div>
  </div>
</div>