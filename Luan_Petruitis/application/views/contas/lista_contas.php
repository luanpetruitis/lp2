<div class="container">
    <div class="mt-4">
      <div class="col-md-2">
        <a
        class="btn btn-primary"
        data-mdb-toggle="collapse"
        href="#collapseForm"
        role="button"
        aria-expanded="false"
        aria-controls="collapse"
        >
        Nova conta
        </a>
      </div>
      <div class="col-md-2 offset-md-7">
        <input type="month" name="month" id="month" value="<?= set_value('month') ?>" />
      </div>
    </div>

    <?php echo form_error('parceiro', '<div class="alert alert-danger">', '</div>'); ?>
    <?php echo form_error('descricao', '<div class="alert alert-danger">', '</div>'); ?>
    <?php echo form_error('valor', '<div class="alert alert-danger">', '</div>'); ?>
    <?php echo form_error('mes', '<div class="alert alert-danger">', '</div>'); ?>
    <?php echo form_error('ano', '<div class="alert alert-danger">', '</div>'); ?>

    <div class="collapse mt-3" id="collapseForm">
    <div class="row">
        <div class="col-md-6 mx-auto mt-5 pt-5">
            <form method="POST" id="contas-form">
                <input id="parceiro" value="<?= set_value('parceiro') ?>" type="text" class="form-control" name="parceiro" placeholder="Devedor / Credor"><br/>
                <input id="descricao" value="<?= set_value('descricao') ?>" type="text" class="form-control" name="descricao" placeholder="Descrição"><br/>
                <input id="valor" value="<?= set_value('valor') ?>" type="number" class="form-control" name="valor" placeholder="Valor"><br/>

                <input type="hidden" name="id" id="conta_id" />
                <input type="hidden" name="tipo" value="<?= $tipo?>" /><br/><br/>

                <div class="text-center text-md-left mb-3">
                    <a class="btn btn-primary" onclick="document.getElementById('contas-form').submit();" >Enviar</a>
                </div>
            </form>
            </div>
        </div>
    </div>

    
    <div class="row">
        <div class="col">
            <?= $lista ?>
        </div>
    </div>
</div>


<div
  class="modal fade"
  id="exampleModal"
  tabindex="-1"
  aria-labelledby="exampleModalLabel"
  aria-hidden="true"
>
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Remoção de Contas</h5>
        <button
          type="button"
          class="btn-close"
          data-mdb-dismiss="modal"
          aria-label="Close"
        ></button>
      </div>
      <div class="modal-body">
        Deseja realmente apagar esta conta? Essa ação é permanente.
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-mdb-dismiss="modal">
          Cancelar
        </button>
        <button type="button"  id="confirmBtn" class="btn btn-primary">Remover</button>
      </div>
    </div>
  </div>
</div>

<script>
  row_id = 0;

  $(document).ready(function() {
    $('#month').change(loadMonth);
    $('.delete_btn').click(openModal);
    $("#confirmBtn").click(deleteRow);
    $(".edit_btn").click(exibeForm);
    $(".pay_btn").click(liquidaConta);
  });

  function loadMonth() {
    var date = this.value.split('-');
    var ano = date[0];
    var mes = date[1];
    var v = window.location.href.split('/');
    
    var url = v.slice(0, 7).join('/');
    url = url + '/' + mes + '/' + ano

    window.location.href = url;
  }

  function exibeForm() {
    var row_id = this.id;
    var td = $('#'+row_id).parent().parent().parent().children();
    
    $('#parceiro').val($(td[0]).text());
    $('#descricao').val($(td[1]).text());
    $('#valor').val($(td[2]).text());
    $('#mes').val($(td[3]).text());
    $('#ano').val($(td[4]).text());
    $('#conta_id').val(row_id);

    $('#collapseForm').collapse('show');
  }

  function openModal(){
    row_id = this.id;
    $("#exampleModal").modal('show');
  }
  
  function deleteRow(){
    var id = row_id;
    $.post(api('contas', 'delete_conta'), {id: id}, (d, s, x) => {
      $('#'+row_id).parent().parent().parent().remove();
      $("#exampleModal").modal('hide');
    });
  }

  function liquidaConta() {
    var id = this.id;
    $.post(api('contas', 'status_conta'), {id: id}, (d, s, x) => {
      $('#' + id).toggleClass('text-muted text-green');
    });
  }

</script>