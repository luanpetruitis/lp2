<nav class="navbar navbar-expand-lg navbar-dark bg-primary">


  <div class="container-fluid">
    
    <a class="navbar-brand" href="#">Controle financeiro</a>


    <button
      class="navbar-toggler"
      type="button"
      data-mdb-toggle="collapse"
      data-mdb-target="#navbarSupportedContent"
      aria-controls="navbarSupportedContent"
      aria-expanded="false"
      aria-label="Toggle navigation"
    >
      <i class="fas fa-bars"></i>
    </button>


    <div class="collapse navbar-collapse" id="navbarSupportedContent">
    
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item">
            <a class="nav-link active" aria-current="page" href="<?= base_url('home') ?>">Home</a>

            <li class="nav-item dropdown">
            <a
                class="nav-link dropdown-toggle"
                href="#"
                id="navbarDropdown"
                role="button"
                data-mdb-toggle="dropdown"
                aria-expanded="false"
            >
                Cadastro
            </a>

            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                <li><a class="dropdown-item" href="<?= base_url('usuario/cadastro') ?>">Usuários</a></li>
                <li><a class="dropdown-item" href="<?= base_url('ContaBancaria/cadastro') ?>">Conta Bancária</a></li>
                <a class="dropdown-item" href="#">Parceiros</a>
                </li>
                
            </ul>
            </li>

            <li class="nav-item dropdown">
                <a
                    class="nav-link dropdown-toggle"
                    href="#"
                    id="navbarDropdown"
                    role="button"
                    data-mdb-toggle="dropdown"
                    aria-expanded="false"
                >
                    Lançamento
                </a>

                <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <li><a class="dropdown-item" href="<?= base_url('contas/pagar')?>">Contas a Pagar</a></li>
                    <li><a class="dropdown-item" href="<?= base_url('contas/receber')?>">Contas a Receber</a></li>
                    <a class="dropdown-item" href="#">Fluxo de Caixa</a>
                    </li>
                </ul>
            </li>
            
            <li class="nav-item dropdown">
                <a
                    class="nav-link dropdown-toggle"
                    href="#"
                    id="navbarDropdown"
                    role="button"
                    data-mdb-toggle="dropdown"
                    aria-expanded="false"
                >
                    Relatórios
                </a>

                <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <li><a class="dropdown-item" href="#">Lançamentos por Período</a></li>
                    <li><a class="dropdown-item" href="<?= base_url('contas/movimento')?>">Movimento de Caixa</a></li>
                    <a class="dropdown-item" href="#">Resumo Anual</a>
            </li>

        </li>
      </ul>

    </div>
   
  </div>
  
</nav>
